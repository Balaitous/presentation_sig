[bash]$ gdalinfo T2m.grib
Driver: GRIB/GRIdded Binary (.grb)
Files: T2m.grib
Size is 801, 601
Coordinate System is:
GEOGCS["Coordinate System imported from GRIB file",
    DATUM["unknown",
        SPHEROID["Sphere",6367470,0]],
    PRIMEM["Greenwich",0],
    UNIT["degree",0.0174532925199433]]
Origin = (-8.012499999999999,53.012500000000003)
Pixel Size = (0.025000000000000,-0.025000000000000)
Corner Coordinates:
Upper Left  (  -8.0125000,  53.0125000) (  8d 0'45.00"W, 53d 0'45.00"N)
Lower Left  (  -8.0125000,  37.9875000) (  8d 0'45.00"W, 37d59'15.00"N)
Upper Right (  12.0125000,  53.0125000) ( 12d 0'45.00"E, 53d 0'45.00"N)
Lower Right (  12.0125000,  37.9875000) ( 12d 0'45.00"E, 37d59'15.00"N)
Center      (   2.0000000,  45.5000000) (  2d 0' 0.00"E, 45d30' 0.00"N)
Band 1 Block=801x1 Type=Float64, ColorInterp=Undefined
  Description = 2[m] HTGL (Specified height level above ground)
  Metadata:
    GRIB_COMMENT=Temperature [K]
    GRIB_ELEMENT=TMP
    GRIB_FORECAST_SECONDS=43200 sec
    GRIB_REF_TIME=  1394409600 sec UTC
    GRIB_SHORT_NAME=2-HTGL
    GRIB_UNIT=[K]
    GRIB_VALID_TIME=  1394452800 sec UTC
