library(c("gstat", "sp", "raster")
# Read temperature data
data = read.table("temperature.csv", sep=',', header=TRUE)
coordinates(data) = ~x+y
proj4string(data) = "+init=epsg:3035"
# Compute variogram
data.variogram = variogram(t~1, data, cutoff=800000)
data.model = fit.variogram(data.variogram, vgm(1, "Exp", 300000, 1))
# Create output grid
topo = GridTopology(c(2500000,1200000), c(10000,10000), c(400,400))
data.grid = SpatialGridDataFrame(topo, data=data.frame(rep(0, 400*400)), proj4string="+init=epsg:3035")
# Do kriging interpolation
data.krig = krige(t~1, data, data.grid, data.model, maxdist=400000)
# Do inverse distance weighted interpolation
data.dist = krige(t~1, data, data.grid)
# Write data to geotif file
writeRaster(raster(data.krig), "temperature_krige.tif", overwrite=TRUE)
writeRaster(raster(data.dist), "temperature_dist.tif", overwrite=TRUE)
