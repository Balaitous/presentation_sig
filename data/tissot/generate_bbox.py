#!/bin/env python3
"""
Création d'une bbox du globe -179.9999, -89.99999, 179.99999, 89.99999
"""

import ogr
import osr

output = 'bbox.shp'
delta = .25


def get_points():
    data = []
    lon = -180
    lat = -90
    data.append((lon, lat))
    while lon < 180:
        lon += delta
        data.append((lon, lat))
    while lat < 90:
        lat += delta
        data.append((lon, lat))
    while lon > -180:
        lon -= delta
        data.append((lon, lat))
    while lat > -90:
        lat -= delta
        data.append((lon, lat))
    data_crop = []
    for lon, lat in data:
        if lon == 180:
            lon = 179.99999
        elif lon == -180:
            lon = -179.99999
        if lat == 90:
            lat = 89.99999
        elif lat == -90:
            lat = -89.99999
        data_crop.append((lon, lat))
    return data_crop

wgs84 = osr.SpatialReference()
wgs84.ImportFromEPSG(4326)

driver = ogr.GetDriverByName('ESRI Shapefile')
ds = driver.CreateDataSource(output)
layer = ds.CreateLayer(output, wgs84, geom_type=ogr.wkbPolygon)

feature = ogr.Feature(feature_def=layer.GetLayerDefn())
wkt = 'POLYGON((%s))' % ','.join("%s %s" % x for x in get_points())
geom = ogr.CreateGeometryFromWkt(wkt)
geom.AssignSpatialReference(wgs84)
feature.SetGeometry(geom)
layer.CreateFeature(feature)

ds.Destroy()
