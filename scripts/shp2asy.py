#!/bin/env python3
import argparse
import math
from osgeo import ogr


def parse_args():
    parser = argparse.ArgumentParser(
        description="Conversion d'un shapefile vers un chemin Asymptote 3D "
        "sur une sphère unitaire."
    )
    parser.add_argument(
        "shapefile",
        help="Fichier shapefile (avec extension .shp)"
    )
    parser.add_argument(
        "--path", "-p",
        help="Nom du chemin",
        required=False,
        default="coastline"
    )

    return parser.parse_args()


def reproject(a, b):
    a = a * math.pi / 180
    b = b * math.pi / 180
    x = math.cos(b) * math.cos(a)
    y = math.cos(b) * math.sin(a)
    z = math.sin(b)
    return (x, y, z)


def to_asy3d(coords, varname):
    print("path3[] %s = {" % varname)
    paths = []
    for line in coords:
        path = "--\n    ".join(["(%f, %f, %f)" % pt for pt in line])
        paths.append(path)
    paths_str = ",\n    ".join(paths)
    print("    %s" % paths_str)
    print("};")


def get_coords3d(layer):
    layer.ResetReading()
    return [
        [reproject(*pt) for pt in feature.GetGeometryRef().GetPoints()]
        for feature in layer
    ]


def to_asy2d(coords, varname):
    print("path[] %s = {" % varname)
    paths = []
    for line in coords:
        path = "--\n    ".join(["(%f, %f)" % pt for pt in line])
        paths.append(path)
    paths_str = ",\n    ".join(paths)
    print("    %s" % paths_str)
    print("};")


def get_coords2d(layer):
    layer.ResetReading()
    return [
        [
            (x * math.pi / 180, y * math.pi / 180)
            for x, y in feature.GetGeometryRef().GetPoints()
        ]
        for feature in layer
    ]


def main():
    args = parse_args()
    driver = ogr.GetDriverByName("ESRI Shapefile")
    ds = driver.Open(args.shapefile)
    layer = ds.GetLayer()

    print("import three;")
    to_asy3d(get_coords3d(layer), args.path)
    to_asy2d(get_coords2d(layer), args.path + "_lonlat")


if __name__ == "__main__":
    main()
