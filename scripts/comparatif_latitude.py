#!/bin/env python3
# -*- coding: utf-8 -*-

import math

# paramètres de l'ellipsoide
a = 6378137
b = 6356752.314245179497563967

psi_list = [30, 45, 60]


def radian(x):
    return x * math.pi / 180.0


def degree(x):
    return x * 180.0 / math.pi


def dms(x):
    deg = int(math.floor(x))
    x = (x - deg) * 60
    if x == 0:
        return "\\azimuth{%i}" % deg
    minutes = int(math.floor(x))
    seconds = int(round((x - minutes) * 60))
    return "\\azimuth{%i;%i;%i}" % (deg, minutes, seconds)


for psi in psi_list:
    psi_r = radian(psi)
    omega = degree(math.atan(b / a * math.tan(psi_r)))
    phi = degree(math.atan(a / b * math.tan(psi_r)))
    print("%s & %s & %s \\\\" % (dms(psi), dms(omega), dms(phi)))
