#!/bin/env python3
import sys
import re


dot = 0.0141111110035
decimal = r'[+-]?\d*((\.\d*)|\d)'
letter = r'[a-zA-Z]+'
coordinates_regex_str = r'\((?P<x>%s),(?P<y>%s)\)' % (decimal, decimal)
coordinates_regex = re.compile(coordinates_regex_str)
unit_regexp_str = 'psset{xunit=(?P<xunit>%s)(?P<xunit_name>%s),' \
                  'yunit=(?P<yunit>%s)(?P<yunit_name>%s),' \
                  'runit=(?P<runit>%s)(?P<runit_name>%s)}' % (
                      decimal, letter, decimal, letter, decimal, letter)
unit_regexp = re.compile(unit_regexp_str)

m = re.search('\{(?P<runit>%s)(?P<runit_name>%s)\}' % (decimal, letter), '{.5pt}')

def get_coordinates(m):
    x = round(float(m.group('x')) * dot / xfactor, 3)
    y = round(float(m.group('y')) * dot / yfactor, 3)
    return x, y

def convert_coor(m):
    x, y = get_coordinates(m)
    if (x, y) in nodes:
        return "(%s)" % nodes[(x, y)]
    if x == int(x):
        x = int(x)
    if y == int(y):
        y = int(y)
    return "(%s,%s)" % (x, y)


# Première lecture Détermine les unités
infile = open(sys.argv[1], 'r')
for line in infile:
    print(line.strip())
    m = unit_regexp.search(line)
    if m is not None:
        xfactor = float(m.group('xunit'))
        yfactor = float(m.group('yunit'))
        break
infile.close()

# Deuxième lecture, Capture des coordonnées
infile = open(sys.argv[1], 'r')
coords_known = {}
for m in coordinates_regex.finditer(infile.read()):
    coords = get_coordinates(m)
    if coords in coords_known:
        coords_known[coords] = coords_known[coords] + 1
    else:
        coords_known[coords] = 1
nodes = [coords for coords, nb in coords_known.items() if nb > 1]
nodes.sort()
nodes = {coords: 'node%i' % i for i, coords in enumerate(nodes)}
infile.close()

# Troisième lecture, Remplacement des coordonnées
infile = open(sys.argv[1], 'r')
for line in infile:
    newline = coordinates_regex.sub(convert_coor, line)
    print(newline.strip())
    if line.startswith("\\begin{pspicture}"):
        nodes_list = [(name, coords) for coords, name in nodes.items()]
        nodes_list.sort(key=lambda x: int(x[0][4:]))
        for name, coords in nodes_list:
            print("\pnode(%s,%s){%s}" % (coords[0], coords[1], name))
infile.close()
