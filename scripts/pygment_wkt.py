#!/bin/env python3
import sys

from pygments import highlight
from pygments.formatters.latex import LatexFormatter
from pygments.lexer import RegexLexer
from pygments.token import Generic
from pygments.token import Keyword
from pygments.token import Name
from pygments.token import Number
from pygments.token import Punctuation
from pygments.token import String
from pygments.token import Text


# Voir: https://svn.osgeo.org/metacrs/sr.org/srsbrowser/views/render.py
class WKTLexer(RegexLexer):
    name = 'wkt'
    aliases = ['wkt']
    filenames = ['*.wkt']

    tokens = {
        'root': [
            (r'\s+', Text),
            (r'[{}\[\]();,-.]+', Punctuation),
            (r'(PROJCS)\b', Generic.Heading),
            (r'(PARAMETER|PROJECTION|SPHEROID|DATUM|GEOGCS|AXIS)\b', Keyword),
            (r'(PRIMEM|UNIT|TOWGS84)\b', Keyword.Constant),
            (r'(AUTHORITY)\b', Name.Builtin),
            (r'[$a-zA-Z_][a-zA-Z0-9_]*', Name.Other),
            (r'[0-9][0-9]*\.[0-9]+([eE][0-9]+)?[fd]?', Number.Float),
            (r'0x[0-9a-fA-F]+', Number.Hex),
            (r'[0-9]+', Number.Integer),
            (r'"(\\\\|\\"|[^"])*"', String.Double),
            (r"'(\\\\|\\'|[^'])*'", String.Single),
        ]
    }


if __name__ == '__main__':
    in_filename = sys.argv[1]
    with open(in_filename, 'r') as infile:
        txt = infile.read()
    pretty_wkt = highlight(
        txt, WKTLexer(), LatexFormatter(commandprefix='PYGdefault'))
    print(pretty_wkt)
