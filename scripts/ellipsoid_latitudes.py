#!/bin/env python3
# -*- coding: utf-8 -*-

import math
from pyproj import transform

from commun_ellipsoid import ellipsoids


def dms(x):
    deg = int(math.floor(x))
    x = (x - deg) * 60
    minutes = int(math.floor(x))
    seconds = int(round((x - minutes) * 60))
    return "\\azimuth{%i;%i;%i}" % (deg, minutes, seconds)


latitudes = [[30.0], [45.0], [60.0]]

src = ellipsoids[0].proj()
for ellipsoid in ellipsoids[1:]:
    dst = ellipsoid.proj()
    for lats in latitudes:
        x, y = transform(src, dst, 0, lats[0])
        lats.append(y)

for i, ellipsoid in enumerate(ellipsoids):
    items = [ellipsoid.name]
    for lats in latitudes:
        items.append(dms(lats[i]))
    print("%s \\\\" % " & ".join(items))
