# -*- coding: utf-8 -*-

from collections import namedtuple

from pyproj import Proj


class Ellipsoid(namedtuple('Ellipsoid', ['name', 'a_str', 'b_str', 'rf_str'])):

    @property
    def a(self):
        return float(self.a_str)

    @property
    def b(self):
        if self.b_str is None:
            f = float(self.rf_str)
            return self.a * (1 - 1 / f)
        else:
            return float(self.b_str)

    @property
    def rf(self):
        if self.rf_str is None:
            b = float(self.b_str)
            try:
                return self.a  / (self.a - b)
            except ZeroDivisionError:
                return None
        else:
            return float(self.rf_str)

    @property
    def proj4_def(self):
        defs = ["+proj=lonlat", "+a=%f" % self.a]
        if self.b_str is not None:
            defs.append("+b=%f" % self.b)
        else:
            defs.append("+rf=%f" % self.rf)
        defs.append("+towgs84=0,0,0")
        return " ".join(defs)

    def proj(self):
        return Proj(self.proj4_def)


ellipsoids = (
    Ellipsoid('WGS 84', "6378137.0", None, "298.257223563"),
    Ellipsoid('Clarke 1880 (IGN)', "6378249.2", "6356515.0", None),
    Ellipsoid('International Hayford 1909', "6378388.0", None, "297"),
    Ellipsoid('Plessis 1817 (France)', "6376523", "6355863", None)
)
