#!/bin/env python3
# -*- coding: utf-8 -*-

from commun_ellipsoid import ellipsoids


def si(x, digits=None, italic=False):
    if x is None:
        return ""
    if digits is not None:
        x = round(float(x), digits)
    if italic:
        return "\\color{gray} %s" % x
    else:
        return "%s" % x


for ellipsoid in ellipsoids:
    items = [ellipsoid.name]
    items.append(si(ellipsoid.a_str))
    if ellipsoid.b_str is not None:
        items.append(si(ellipsoid.b_str))
    else:
        items.append(si(ellipsoid.b, 3, True))
    if ellipsoid.rf_str is not None:
        items.append(si(ellipsoid.rf_str))
    else:
        items.append(si(ellipsoid.rf, 6, True))
    print("%s \\\\" % " & ".join(items))
