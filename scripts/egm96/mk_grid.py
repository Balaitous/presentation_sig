#!/bin/env python3
delta = .25

lon = -180

while lon <= 180:
    lat = -90
    while lat <= 90:
        print("%8.2f %8.2f" % (lat, lon))
        lat += delta
    lon += delta
