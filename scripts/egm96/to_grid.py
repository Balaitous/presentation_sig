#!/bin/env python3
import gdal
import osr
import re
import numpy as np

delta = .25
nx = int(360 / delta + 1)
ny = int(180 / delta + 1)
data = np.zeros((ny, nx), np.float32)

infile = open("OUTF477.DAT", 'r')
for line in infile.readlines():
    lat, lon, ele = [float(x) for x in re.split("\s+", line.strip())]
    i = int((lon + 180) / delta)
    j = ny - int((lat + 90) / delta) - 1
    data[j, i] = ele

driver = gdal.GetDriverByName("GTiff")
layer = driver.Create('egm96.tif', nx, ny, 1, gdal.GDT_Float32)
layer.SetGeoTransform([-180 - delta / 2, delta, 0,
                       90 + delta / 2, 0, -delta])

srs = osr.SpatialReference()
srs.ImportFromEPSG(4326)
layer.SetProjection(srs.ExportToWkt())

band = layer.GetRasterBand(1)
band.WriteArray(data)
