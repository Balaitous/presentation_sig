import coastline;

size(12cm,0);

pen graticule_pen = rgb(153/255, 92/255, 0);

draw(coastline_lonlat, darkcyan);
for(int i = 0; i < 60; ++i) {
  real lonmin = (6*i - 180) * pi / 180;
  real lonmax = lonmin + 6 * pi/180;
  path bboxN = box((lonmin, 0), (lonmax, pi/2));
  draw(bboxN, graticule_pen);
  Label lbl = scale(.6) * rotate(90) * Label(string(i+1) + " N");
  draw(lbl, ((lonmin + lonmax) / 2, pi/4), nullpen);
  path bboxS = box((lonmin, -pi/2), (lonmax, 0));
  draw(bboxS, graticule_pen);
  Label lbl = scale(.6) * rotate(90) * Label(string(i+1) + " S");
  draw(lbl, ((lonmin + lonmax) / 2, -pi/4), nullpen);
}
draw(box((-pi,-pi/2), (pi,pi/2)), linewidth(1));
