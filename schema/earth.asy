private import three;
private import solids;
private import projections;
private import coastline;


triple pS        = (0, 0, -1); // Pôle sud
triple pN        = (0, 0, 1);  // Pôle nord
triple center    = (0, 0, 0);  // Centre de la Terre
int n_transverse = 13;         // Nombre de parallèles


/**
 * Initialisation des paramètres graphiques (Schéma 3D)
 */
void init_graph3d(real width                  = 5cm,
                  real height                 = 0,
                  projection graph_projection = orthographic(5,2,2.5)) {
  size(width, height);
  currentprojection = graph_projection;
}


/**
 * Initialisation des paramètres graphiques (Schémas 2D)
 */
void init_graph2d(real width  = 10cm,
                  real height = 0) {
  size(width, height);
}


/**
 * Opération de cloture du graphique 2D
 */
void end_graph2d() {
  shipout(bbox(white, Fill));
}


/*******************************************************************************
 * Représentation de la Terre
 ******************************************************************************/

struct Earth {
  // Définition des couleurs
  pen earth_front = darkgreen;
  pen earth_back = earth_front + linetype("10 10",10) + opacity(.3);
  pen projected_front = darkblue;
  pen projected_back = projected_front + linetype("10 10",10) + opacity(.3);

  bool show_example_point;

  int nslice = 36;
  revolution earth = sphere(center, 1, n=nslice);


  void operator init(bool show_example_point=true) {
    this.show_example_point = show_example_point;
  }

  /**
   * Affichage en 3D de la sphère avec les traits de côtes.
   */
  void draw_sphere(path3 x_axe = (0,0,0)--(2,0,0),
                   path3 y_axe = (0,0,0)--(0,1.2,0),
                   path3 z_axe = (0,0,0)--(0,0,1.5)) {
    // Tracé des axes
    draw(Label("$x$", EndPoint), x_axe, Arrow3());
    draw(Label("$y$", EndPoint), y_axe, Arrow3());
    draw(Label("$z$", EndPoint), z_axe, Arrow3());

    // Tracé des surfaces
    draw(surface(earth), earth_front + opacity(.5));

    // Tracé du squelette
    skeleton earth_sk = earth.skeleton(m=n_transverse, n=12, P=currentprojection);
    draw(earth_sk.transverse.back, earth_back);
    draw(earth_sk.longitudinal.back, earth_back);
    draw(earth_sk.transverse.front, earth_front);
    draw(earth_sk.longitudinal.front, earth_front);

    // Trait de côte
    draw(lonlat2xyz(coastline_lonlat), earth_front);
  }

  /**
   * Affichage d'un cercle de latitude
   * @param lat Latitude (degrés)
   */
  void draw_latitude(real lat) {
    skeleton s;
    real factor = (.5 + lat / 180) * nslice;
    earth.transverse(s, factor, n=12, P=currentprojection);
    draw(s.transverse.front, red + linewidth(1));
    draw(s.transverse.back, red + linewidth(1) + linetype("10 10",10));
  }

  /**
   * Schématisation 3D des coordonnées (lon, lat)
   */
  void draw_coordinates(real lon, real lat, real arrowsize=5) {
    triple M = lonlat2xyz((lon, lat));
    triple Mx = (M.x, 0, 0);
    triple My = (0, M.y, 0);
    triple Mz = (0, 0, M.z);
    triple H = Mx + My;
    draw((0,0,0)--M);
    draw((0,0,0)--H--M, dashed);
    draw(M--Mz, dashed);
    real lon_d = lon * 180 / pi;
    real lat_d = lat * 180 / pi;
    real r = length(H);
    draw(Label("$\lambda$", MidPoint),
         arc((0,0,0), r, 90, 0, 90, lon_d),
         Arrow3(size=arrowsize));
    draw(Label("$\varphi$", MidPoint),
         arc((0,0,0), r, 90, lon_d, 90 - lat_d, lon_d, direction=CCW),
         Arrow3(size=arrowsize));
  }

  /**
   * Affichage en 3D de la surface de projection.
   */
  void draw_projection(map_projection proj,
                       real lonM = 70*pi/180,
                       real latM = 45*pi/180) {
    if(show_example_point) {
      // Symbolisation d'un point de la projection
      draw_coordinates(lonM, latM);
      path3 l   = proj.repr((lonM, latM));
      draw(l, red + linewidth(1));
      dot(l, red + linewidth(2));
      draw(Label("$M$", Relative(.999)), subpath(l, 0, 1), nullpen);
      draw(Label("$M^\prime$", EndPoint), l, nullpen);
    }

    // Tracé de la surface de projection
    draw(proj.surface(), projected_front + opacity(.2));

    // Tracé du squelette
    skeleton sk = proj.skeleton(P=currentprojection);
    draw(sk.transverse.back, projected_back);
    draw(sk.longitudinal.back, projected_back);
    draw(sk.transverse.front, projected_front);
    draw(sk.longitudinal.front, projected_front);

    // Trait de côte
    real coastline_opacity = .15;
    path[] c = proj.crop(coastline_lonlat);
    draw(proj.project3d(c), projected_front + opacity(coastline_opacity));
  }
}


/**
 * Retourne les méridiens et les parallèles
 * @param delta_lon Écart entre 2 méridiens (si 0 delta_lon = delta_lat
 * @param delta_lon Écart entre 2 parallèles (si 0 delta_lat = delta_lon
 */
path[] graticule(real delta_lon=0, real delta_lat=0, real step=2) {
  if(delta_lon == 0 && delta_lat == 0) {
    delta_lon = delta_lat = 30;
  } else if(delta_lon == 0) {
    delta_lon = delta_lat;
  } else if(delta_lat == 0) {
    delta_lat = delta_lon;
  }
  path[] result;
  for(real lon = -180; lon <= 180; lon += delta_lon) {
    path p;
    for(real lat = -90; lat <= 90; lat += step) {
      p = p--(lon * pi / 180, lat * pi / 180);
    }
    result.push(p);
  }
  for(real lat = -90; lat <= 90; lat += delta_lat) {
    path p;
    for(real lon = -180; lon <= 180; lon += step) {
      p = p--(lon * pi / 180, lat * pi / 180);
    }
    result.push(p);
  }
  return result;
}


struct Graph2D {

  map_projection proj;

  void operator init(map_projection proj) {
    this.proj = proj;
  }

  void draw() {
    import coastline;
    draw(proj.project2d(proj.crop(coastline_lonlat)), darkcyan);
    draw(proj.project2d(proj.crop(graticule(30, 30, 2))), orange);
    draw(proj.bbox2d(), black);
  }
}
