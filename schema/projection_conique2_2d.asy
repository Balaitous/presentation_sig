import earth;
import projections;

init_graph2d();

// Paramètres
real lat0 = 30 * pi / 180;
real lat1 = 60 * pi / 180;

Graph2D graph = Graph2D(conique2(lat0, lat1));
graph.draw();

end_graph2d();
