private import three;
private import solids;


/**
 * Retourne les parties de p contenues dans le chemin fermé q.
 */
path[] cutline(path p, path q) {
  path[] result;
  real[][] t = intersections(p, q);
  t.push(new real[] {size(p), 0});
  int N = t.length;
  real t0 = 0;
  for(int i = 0; i < N; ++i) {
    real t1 = t[i][0];
    pair middle = point(p, (t0 + t1) / 2);
    if(inside(q, middle)) {
      result.push(subpath(p, t0, t1));
    }
    t0 = t1;
  }
  return result;
}

path[] cutline(path p, path[] q) {
  path[] result;
  real[] t;
  for(path qi: q) {
    for(real[] ti: intersections(p, qi)) {
      t.push(ti[0]);
    }
  }
  t = sort(t);
  t.push(size(p)-1);
  int N = t.length;
  real t0 = 0;
  for(int i = 0; i < N; ++i) {
    real t1 = t[i];
    if(t0 == t1) {
      continue;
    }
    pair middle = point(p, (t0 + t1) / 2);
    if(inside(q, middle)) {
      result.push(subpath(p, t0, t1));
    }
    t0 = t1;
  }
  return result;
}

path[] cutline(path[] p, path[] q) {
  path[] result;
  for(path item: p) {
    result = result^^cutline(item, q);
  }
  return result;
}


/**
 * Transformation de coordonnées (lon, lat) -> (x, y, z)
 * @param pt (lon, lat)
 */
triple lonlat2xyz(pair pt) {
  return (cos(pt.y) * cos(pt.x),
          cos(pt.y) * sin(pt.x),
          sin(pt.y));
}

path3 lonlat2xyz(path p) {
  int N = size(p);
  path3 result;
  for(int i = 0; i < N; ++i) {
    result = result -- lonlat2xyz(point(p, i));
  }
  return result;
}

path3[] lonlat2xyz(path[] p) {
  path3[] result;
  for(path item: p) {
    result.push(lonlat2xyz(item));
  }
  return result;
}


/**
 * Transformation de coordonnées (x, y, z) -> (lon, lat)
 */
pair xyz2lonlat(triple pt) {
  return (atan2(pt.y, pt.x),
          atan2(pt.z, sqrt(pt.x^2 + pt.y^2)));
}

path xyz2lonlat(path3 p) {
  int N = size(p);
  path result;
  for(int i = 0; i < N; ++i) {
    result = result -- xyz2lonlat(point(p, i));
  }
  return result;
}

path[] xyz2lonlat(path3[] p) {
  path[] result;
  for(path3 item: p) {
    result.push(xyz2lonlat(item));
  }
  return result;
}

/**
 * Transformation de coordonnées (lon, lat) du repère xyz vers zxy
 */
pair xyz2zxy(pair pt) {
  triple M = lonlat2xyz(pt);
  return xyz2lonlat((M.z, M.x, M.y));
}

path xyz2zxy(path p) {
  int N = size(p);
  path result;
  for(int i = 0; i < N; ++i) {
    result = result -- xyz2zxy(point(p, i));
  }
  return result;
}

path[] xyz2zxy(path[] p) {
  path[] result;
  for(path item: p) {
    result.push(xyz2zxy(item));
  }
  return result;
}

/**
 * Transformation de coordonnées (lon, lat) du repère zxy vers xyz
 */
pair zxy2xyz(pair pt) {
  triple M = lonlat2xyz(pt);
  return xyz2lonlat((M.y, M.z, M.x));
}

path zxy2xyz(path p) {
  int N = size(p);
  path result;
  for(int i = 0; i < N; ++i) {
    result = result -- zxy2xyz(point(p, i));
  }
  return result;
}

path[] zxy2xyz(path[] p) {
  path[] result;
  for(path item: p) {
    result.push(zxy2xyz(item));
  }
  return result;
}

/**
 * Projection cartographique
 */
struct map_projection {
  /**
   * Projection d'un point sur le plan
   * @param pt (lon, lat)
   */
  pair project2d(pair pt);

  path project2d(path p) {
    int N = size(p);
    path result;
    for(int i = 0; i < N; ++i) {
      result = result -- project2d(point(p, i));
    }
    return result;
  }

  path[] project2d(path[] p) {
    path[] result;
    for(path item: p) {
      result.push(project2d(item));
    }
    return result;
  }

  /**
   * Projection dans l'espace
   * @param pt (lon, lat)
   */
  triple project3d(pair pt);

  path3 project3d(path p) {
    int N = size(p);
    path3 result;
    for(int i = 0; i < N; ++i) {
      result = result -- project3d(point(p, i));
    }
    return result;
  }

  path3[] project3d(path[] p) {
    path3[] result;
    for(path item: p) {
      result.push(project3d(item));
    }
    return result;
  }

  /**
   * Repésentation 3D de la surface de projection
   */
  surface surface();

  /**
   * Repésentation 3D du squelette de la surface de projection
   */
  skeleton skeleton(projection P);

  /**
   * Extrait les parties des chemins représentables sur le plan de projection
   * @param p Chemins en coordonnées géographiques (lon, lat)
   */
  path[] crop(path p) {
    return p;
  }

  path[] crop(path[] p) {
    path[] result;
    for(path item: p) {
      result = concat(result, crop(item));
    }
    return result;
  }

  /**
   * Contour du domaine après projection sur le plan
   */
  path bbox2d();

  /**
   * Ligne 0--M--Mp symbolisant la projection du point M
   * @param M (lon, lat) Coordonnées du point à symboliser
   */
  path3 repr(pair M) {
    return (0, 0, 0)--lonlat2xyz(M)--project3d(M);
  }

}


/**
 * Projection cylindrique de Lambert
 * Projection cylindrique orthogonale de la sphère
 */
map_projection cylindrique_lambert() {
  map_projection proj = new map_projection;

  proj.project2d = new pair(pair pt) {
    return (pt.x, sin(pt.y));
  };

  proj.project3d = new triple(pair pt) {
    return (cos(pt.x),
            sin(pt.x),
            sin(pt.y));
  };

  proj.surface = new surface() {
    revolution r = cylinder((0, 0, -1), 1, 2, Z);
    return surface(r);
  };

  proj.skeleton = new skeleton(projection P) {
    revolution r = cylinder((0, 0, -1), 1, 2, Z);
    return r.skeleton(P);
  };

  proj.bbox2d = new path() {
    path bbox = (-pi,-pi/2)--(pi,-pi/2)--(pi,pi/2)--(-pi,pi/2)--(-pi,-pi/2);
    return proj.project2d(bbox);
  };

  proj.repr = new path3(pair M) {
    triple M3d = lonlat2xyz(M);
    return (0, 0, M3d.z)--M3d--proj.project3d(M);
  };

  return proj;
}


/**
 * Projection cylindrique de Lambert
 * Projection cylindrique orthogonale de la sphère
 * @param ymax Demie-hauteur du cylindre de projection
 * @param lon0 Méridien selon lequel le cylindre est tangent (radian)
 */
map_projection transverse_mercator(real ymax=1.5,
                                   real lon0=0) {
  map_projection proj = new map_projection;

  // Latitude maximale dans le repère zxy
  real phi_max = 2 * atan(exp(ymax)) - pi/2;

  // Opérateur de rotation
  transform to_lon0 = shift(-lon0, 0);
  transform from_lon0 = shift(lon0, 0);
  transform3 rot_lon0 = rotate(lon0*180/pi, Z);

  // Surface de révolution de la projection
  revolution r = rot_lon0 * cylinder((0, -ymax, 0), 1, 2 * ymax, Y);

  proj.project2d = new pair(pair pt) {
    pair M = xyz2zxy(to_lon0 * pt);
    if(M.x < -pi/2) {
      M = (M.x + 2*pi, M.y);
    }
    return (log(tan(pi/4 + M.y/2)),
            -M.x + pi/2);
  };

  proj.project3d = new triple(pair pt) {
    pair M = xyz2zxy(to_lon0 * pt);
    return rot_lon0 * (sin(M.x),
                       log(tan(pi/4 + M.y/2)),
                       cos(M.x));
  };

  proj.surface = new surface() {
    return surface(r);
  };

  proj.skeleton = new skeleton(projection P) {
    return r.skeleton(P);
  };

  proj.crop = new path[](path p) {
    real eps = .001;
    path[] bbox_eq = box((-pi,-pi/2), (pi,pi/2));
    if(lon0 >= -pi/2) {
      real latmin = max(lon0-3*pi/2, -pi);
      bbox_eq = bbox_eq ^^ reverse(box((latmin,-eps), (lon0-pi/2+eps,eps)));
    }
    if(lon0 <= pi/2) {
      real latmax = min(lon0+3*pi/2, pi);
      bbox_eq = bbox_eq ^^ reverse(box((lon0+pi/2-eps,-eps), (latmax+eps,eps)));
    }
    path[] tmp = xyz2zxy(to_lon0 * cutline(p, bbox_eq));
    path bbox = (-pi,-phi_max)--(pi,-phi_max)--(pi,phi_max)--(-pi,phi_max)--cycle;
    return from_lon0 * zxy2xyz(cutline(tmp, bbox));
  };

  proj.bbox2d = new path() {
    path bbox = (-ymax,-pi)--(ymax,-pi)--(ymax,pi)--(-ymax,pi)--cycle;
    return bbox;
  };

  proj.repr = new path3(pair M) {
    triple M3d = lonlat2xyz(M);
    return (0, 0, M3d.z)--M3d--proj.project3d(M);
  };

  return proj;
}


/**
 * Projection conique
 * @param lat0 Latitude du parallèle tangent.
 */
map_projection conique(real lat0,
                       real lon0=0) {
  map_projection proj = new map_projection;

  // Rotation des coordonnées géographiques
  transform to_lon0 = shift(-lon0, 0);  // Place le méridien origine à la longitude 0
  transform from_lon0 = shift(lon0, 0);

  proj.project2d = new pair(pair pt) {
    pt = to_lon0 * pt;
    real rho = tan(pi/2 - lat0) - tan(pt.y - lat0);
    if(rho == 0) {
      return (0, 0);
    }
    real theta = cos(pt.y) / cos(pt.y - lat0) * pt.x / rho;
    return (rho * sin(theta),
            -rho * cos(theta));
  };

  proj.project3d = new triple(pair pt) {
    pt = to_lon0 * pt;
    real r = 1 / cos(pt.y - lat0);
    return (r * cos(pt.y) * cos(pt.x),
            r * cos(pt.y) * sin(pt.x),
            r * sin(pt.y));
  };

  proj.surface = new surface() {
    triple sommet = (0, 0, 1 / cos(pi/2 - lat0)); // Sommet du cône de projection
    triple base   = (1 / cos(lat0), 0, 0);        // Point de la base du cône
    revolution r  = revolution(sommet, sommet--base, Z);
    return surface(r);
  };

  proj.skeleton = new skeleton(projection P) {
    triple sommet = (0, 0, 1 / cos(pi/2 - lat0)); // Sommet du cône de projection
    triple base   = (1 / cos(lat0), 0, 0);        // Point de la base du cône
    revolution r  = revolution(sommet, sommet--base, Z);
    return r.skeleton(P);
  };

  proj.crop = new path[](path p) {
    path bbox = (-pi, 0)--(pi, 0)--(pi, pi/2)--(-pi, pi/2)--cycle;
    return cutline(p, bbox);
  };

  proj.bbox2d = new path() {
    pair P1 = proj.project2d((-pi,0));
    real a1 = atan2(P1.y, P1.x) * 180 / pi - 360;
    pair P2 = proj.project2d((pi,0));
    real a2 = atan2(P2.y, P2.x) * 180 / pi;
    real r = length(P1);
    return (0,0)--arc((0,0), r, a1, a2)--cycle;
  };

  return proj;
}


/**
 * Projection conique à deux parallèles tangents (parallèles automécoïques φ1 et φ2)
 * @param lat0 Latitude du premier parallèle tangent.
 * @param lat1 Latitude du deuxième parallèle tangent.
 */
map_projection conique2(real lat0, real lat1,
                       real lon0=0) {
  map_projection proj = new map_projection;

  real alpha   = (lat0 + lat1) / 2;
  real r0      = cos((lat1 - lat0) / 2);
  real x0      = r0 / cos(alpha);
  real z0      = r0 / sin(alpha);
  revolution r = revolution((0, 0, z0),
                            (0, 0, z0)--(x0, 0, 0));
                            // new real(real x) {return z0 - z0/x0*x;},
                            // 0, 1, 30);

  proj.project2d = new pair(pair pt) {
    real rho   = r0 * (tan(pi/2 - alpha) - tan(pt.y - alpha));
    if(rho == 0) {
      return (0, 0);
    }
    real theta = cos(pt.y) / cos(pt.y - alpha) * pt.x * r0 / rho;
    pair result = (rho * sin(theta),
                   -rho * cos(theta));
    return result;
  };

  proj.project3d = new triple(pair pt) {
    real r = r0 / cos(pt.y - alpha);
    return (r * cos(pt.y) * cos(pt.x),
            r * cos(pt.y) * sin(pt.x),
            r * sin(pt.y));
  };

  proj.surface = new surface() {
    return surface(r);
  };

  proj.skeleton = new skeleton(projection P) {
    return r.skeleton(P);
  };

  proj.crop = new path[](path p) {
    path bbox = (-pi, 0)--(pi, 0)--(pi, pi/2)--(-pi, pi/2)--cycle;
    return cutline(p, bbox);
  };

  proj.bbox2d = new path() {
    pair P1 = proj.project2d((-pi,0));
    real a1 = atan2(P1.y, P1.x) * 180 / pi - 360;
    pair P2 = proj.project2d((pi,0));
    real a2 = atan2(P2.y, P2.x) * 180 / pi;
    real r = length(P1);
    return (0,0)--arc((0,0), r, a1, a2)--cycle;
  };

  return proj;
}


/**
 * Projection azimutale
 * @param z0 Coordonnées sur l'axe des pôles du point servant à effectuer la projection
 */
map_projection azimuthal(real z0) {
  map_projection proj = new map_projection;
  real radius = 1.5; // Rayon du disque de projection

  proj.project2d = new pair(pair pt) {
    real r = (1 - z0) / (sin(pt.y) - z0) * cos(pt.y);
    return (r * sin(pt.x),
            -r * cos(pt.x));
  };

  proj.project3d = new triple(pair pt) {
    real r = (1 - z0) / (sin(pt.y) - z0) * cos(pt.y);
    return (r * cos(pt.x),
            r * sin(pt.x),
            1);
  };

  proj.surface = new surface() {
    return surface(circle((0,0,1), radius));
  };

  proj.skeleton = new skeleton(projection P) {
    skeleton sk;
    sk.transverse.front.push(circle((0,0,1), radius));
    return sk;
  };

  proj.crop = new path[](path p) {
    pair[] I = intersectionpoints(circle((0,0), 1), (0,z0)--(radius,1));
    if(I.length == 0) {
      return p;
    } else {
      real lat0 = atan2(I[0].y, I[0].x);
      path bbox = (-pi, lat0)--(pi, lat0)--(pi, pi/2)--(-pi, pi/2)--cycle;
      return cutline(p, bbox);
    }
  };

  proj.bbox2d = new path() {
    return circle((0,0), 1.5);
  };

  proj.repr = new path3(pair M) {
    return (0, 0, -1)--lonlat2xyz(M)--proj.project3d(M);
  };

  return proj;
}
