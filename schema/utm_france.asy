import projections;
import coastline;
import earth;

size(6cm,0);

transform to_rad = scale(pi/180);
map_projection proj = conique(45 * pi/180, 0);

pair pt_SO = proj.project2d(to_rad * (-6,40));
pair pt_SE = proj.project2d(to_rad * (12,40));
pair pt_N = proj.project2d(to_rad * (3, 55));

real xmin = pt_SO.x;
real xmax = pt_SE.x;
real ymin = pt_SO.y;
real ymax = pt_N.y;

path bbox = box((xmin, ymin), (xmax, ymax));

path[] c = proj.project2d(coastline_lonlat);
c = cutline(c, bbox);

path[] g = proj.project2d(graticule(6, 5));
g = cutline(g, bbox);

draw(c, darkcyan);
draw(g, rgb(153/255, 92/255, 0));

draw("30 N", proj.project2d(to_rad * (-3,47.5)));
draw("31 N", proj.project2d(to_rad * (3,47.5)));
draw("32 N", proj.project2d(to_rad * (9,47.5)));

draw(bbox);
