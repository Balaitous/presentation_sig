import three;
import earth;
import projections;

init_graph3d();

Earth earth = Earth(show_example_point=false);
earth.draw_sphere();
earth.draw_projection(transverse_mercator());
