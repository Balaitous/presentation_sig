import three;
import earth;
import projections;

init_graph3d();

Earth earth = Earth();
earth.draw_sphere();
earth.draw_projection(cylindrique_lambert());
