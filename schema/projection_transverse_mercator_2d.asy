import earth;
import projections;

init_graph2d();

// Paramètres
real lat0 = 30 * pi / 180;

Graph2D graph = Graph2D(transverse_mercator());
graph.draw();

end_graph2d();
