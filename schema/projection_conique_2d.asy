import earth;
import projections;

init_graph2d();

// Paramètres
real lat0 = 45 * pi / 180;

Graph2D graph = Graph2D(conique(lat0));
graph.draw();

end_graph2d();
