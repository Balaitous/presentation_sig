import three;
import earth;
import projections;

init_graph3d();

// Paramètres
real lat0 = 30;
real lat1 = 60;

Earth earth = Earth();
earth.draw_sphere();
earth.draw_projection(conique2(lat0 * pi / 180, lat1 * pi / 180));
