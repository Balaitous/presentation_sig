import three;
import earth;
import projections;

init_graph3d();

// Paramètres
real lat0 = 45 * pi / 180; // Latitude du parallèle tangent

Earth earth = Earth();
earth.draw_sphere();
earth.draw_projection(conique(lat0),
                      latM = 60 * pi / 180);
