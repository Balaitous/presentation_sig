## Licence ##

La version complète (pdf) de cette présentation est placée sous licence Creative Commons CC-BY-NC-ND
(http://creativecommons.org/licenses/by-nc-nd/3.0/fr/legalcode).

Les logos Météo-France, Les illustrations de couverture (Première page et page FIN), les illustrations provenant de parties tierces, sont soumis à leur droit d'auteur respectifs.

Les autres éléments de la présentation ne provenant pas de parties tierces sont individuellement placés sous licence CC-BY-NC-SA (http://creativecommons.org/licenses/by-nc-sa/3.0/fr/legalcode), et peuvent ainsi être diffusés et modifiés dans le respect de cette licence.


## Attribution des illustrations ##


* `AMDAR_geoloc`:
  * url: http://www.openstreetmap.org
  * Licence: Creative Commons paternité – partage à l’identique 2.0 (CC-BY-SA)
  * Crédit: © Les contributeurs d’OpenStreetM
* `arome_t2m_lbrt93`, `arome_t2m_wgs84`:
  * Crédit: : © Météo-France
* `aster`:
  * Crédit: ASTER GDEM is a product of METI and NASA.
* `atlas_catalan`:
  * http://commons.wikimedia.org/wiki/File:Europe_Mediterranean_Catalan_Atlas.jpeg
  * Domaine public
* `atlas_ortelius` :
  * http://commons.wikimedia.org/wiki/File:Atlas_Ortelius_KB_PPN369376781-016av-016br.jpg
  * Domaine public
* `vaugondy_1750`:
  * http://commons.wikimedia.org/wiki/File:Vaugondy_1750_-_Le_Royaume_de_France.jpg
  * Domaine public
* `bulletin_meteo_2015_03_08`:
  * url: http://www.meteofrance.com
  * Tout droit réservés (http://www.meteofrance.com/droits-de-reproduction)
  * Crédit: © Météo-France
* `carte_babylonienne_interpretation`:
  * url: http://www.laboiteverte.fr/la-plus-vieille-carte-du-monde/
  * Licence inconnue, de source secondaire
* ̀carte_babylonienne`:
  * url: http://commons.wikimedia.org/wiki/File:Baylonianmaps.JPG
  * Domaine public
* ̀carte_babylonienne_schema`:
  * url: http://commons.wikimedia.org/wiki/File:BabylonianWorldMap2.jpg
  * Domaine public
* `carte_erastotene`:
  * http://commons.wikimedia.org/wiki/File:Mappa_di_Eratostene.png
  * Domaine public
* `carte_ptolomee`:
  * Source primaire: http://www.bl.uk/catalogues/illuminatedmanuscripts/ILLUMIN.ASP?Size=mid&IllID=23282
  * Source secondaire: http://commons.wikimedia.org/wiki/File:PtolemyWorldMap.jpg
  * Domaine public
* `france_1553_nova_totius`:
  * url: http://gallica.bnf.fr/ark:/12148/btv1b72002306
  * Domaine public
* `geoids_sm` :
  * http://commons.wikimedia.org/wiki/File:Geoids_sm.jpg
  * Domaine public (http://www.jsc.nasa.gov/policies.html#Guidelines)
* `google-earth_global`, `google-earth_3d`:
  * Fair use (http://www.google.fr/earth/media/licensing.html)
  * Crédit: © Google
* `paris-geofabrik-xxxx`:
  * url: http://www.geofabrik.de/gallery/history/#paris
  * Licence: CC-BY-SA
* `osm-foix`:
  * url: http://www.openstreetmap.org
  * Licence: CC-BY-SA, Map data from OpenStreetMap is licensed under the ODbL
  * Crédit: ©OpenStreetMap contributors
* `osm-foix_mapquest`:
  * url: http://www.mapquest.com/
  * Crédit: Maps ©MapQuest, Data ©OpenStreetMap contributors
* `osm-foix_cycle`:
  * url: http://www.thunderforest.com/opencyclemap/
  * Licence: CC-BY-SA 2.0, Map data from OpenStreetMap is licensed under the ODbL
  * Crédit: Maps ©Thunderforest, Data ©OpenStreetMap contributors
* `osm-foix_wanderreitkarte`
  * url: http://www.wanderreitkarte.de
  * Licence: CC-BY-NC-SA (http://www.wanderreitkarte.de/licence_de.php), Map data from OpenStreetMap is licensed under the ODbL
  * Crédits: Nop's Reit-&Wanderkarte Daten OSM DEM CIAT
* `vaugondy_1750`:
  * url: http://commons.wikimedia.org/wiki/File:Vaugondy_1750_-_Le_Royaume_de_France.jpg
  * Licence: Domaine public


## Cartes ##

Données utilisées pour la réalisation des cartes d'illustration :

* `corine`:
  * CORINE Land Cover: Couche CLC06_RGF, ©Agence européenne pour l'environnement,
  * geonames: ©GeoNames, CC-BY
* `CC-9zones`:
  * GEOFLA Département ed 2014, ©IGN, Licence ouverte IOOI
  * Fond de carte MapQuest Open, ©MapQuest, données cartographiques ©OpenStreetMap contributors
* `egm96_mollweide`:
  * EGM96: ©Nasa, Domaine public (http://earth-info.nga.mil/GandG/wgs84/gravitymod/egm96/egm96.html)
* `met10_201403121200`:
  * Composition colorée météosat: ©Météo-France
* `meteopole`:
  * Orthophotoplan 2011: ©ToulouseMetropole.data, Licence ODbl
* `rivers`:
  * BD Carthage, Licence Ouverte Etalab
* ̀scan1000`:
  * Crédit:  ©Institut Géographique National (France)
* `T2m`:
  * ERA INTERIM: ©European Centre for Medium-Range Weather Forecasts
