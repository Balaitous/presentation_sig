\section{Repérer des points sur la Terre}

\subsection{Coordonnées bi-dimensionnelles}

\begin{frame}
  \frametitle{Repérer un point dans l'espace: Coordonnées cartésiennes}
  \begin{columns}
    \begin{column}{.5\linewidth}
      $\left(O; \vec\imath, \vec\jmath, \vec{k}\right)$, repère orthonormé :
      \begin{itemize}
      \item $O$ $\approx$ centre de masse.
      \item $\vec{k}$ $\approx$ axe des p\^oles.
      \item $\left(O; \vec\imath, \vec{k}\right)$ $\approx$ plan méridien de Greenwich.
      \end{itemize}
      Coordonnées cartésiennes :
      $$M = \left[ \begin{array}{c} x \\ y \\ z \end{array} \right]$$
    \end{column}
    \begin{column}{.5\linewidth}
      \psset{unit=1.25cm}
      \input{schema/referentiel.tex}
    \end{column}
  \end{columns}
\end{frame}


\begin{frame}
  \frametitle{Repérer un point dans l'espace: Coordonnées sphériques}
  \begin{columns}
    \begin{column}{.5\linewidth}
      \includegraphics[width=\linewidth]{images/coords_spheriques.eps}
    \end{column}
    \begin{column}{.5\linewidth}
      $\rho$: rayon \\
      $\lambda$: latitude paramétrique \\
      $\theta$: longitude
      $$M \left\{
      \begin{array}{l}
        x = \rho\cos\lambda\cos\theta \\
        y = \rho\cos\lambda\sin\theta \\
        z = \rho\sin\lambda
      \end{array}
      \right.$$
    \end{column}
  \end{columns}
\end{frame}


\begin{frame}
  \frametitle{Approximation mathématique: l'ellipsoïde}
  \begin{columns}
    \begin{column}{.5\linewidth}
      \psset{unit=.8cm}
      \input{schema/gravity.tex}
    \end{column}
    \begin{column}{.5\linewidth}
      Pesanteur = Résultante de :
      \begin{itemize}
      \item Force de gravitation
      \item Force centrifuge
      \end{itemize}
      La Terre a la forme d'un ellipsoïde définie par :
      \begin{itemize}
      \item $a$: Demi-grand axe
      \item $b$: Demi-petit axe
      \end{itemize}
      On utilise également l'aplatissement: $f = \cfrac{a-b}{a}$
    \end{column}
  \end{columns}
\end{frame}


\begin{frame}
  \frametitle{Coordonnées paramétriques}
  \begin{columns}
    \begin{column}{.45\linewidth}
      \psset{unit=.7cm}
      \input{schema/parametriques_2d.tex}
      Paramétrisation de l'ellipse:
      $$M \left[ \begin{array}{l} a \cos \psi \\ b \sin\psi \end{array} \right]$$
    \end{column}
    \begin{column}{.55\linewidth}
      Repérage d'un point $M$ par ses coordonnées paramétriques $\left(\lambda, \psi\right)$.
      $$M \left\{
      \begin{array}{l}
        x = a \cos\psi \cos\lambda \\
        y = a \cos\psi \sin\lambda \\
        z = b \sin\psi
      \end{array}
      \right.$$
      \psset{unit=.95cm}
      \centerline{\input{schema/parametriques_3d.tex}}
    \end{column}
  \end{columns}
\end{frame}


\begin{frame}
  \frametitle{Coordonnées géocentriques}
  \begin{columns}
    \begin{column}{.45\linewidth}
      \psset{unit=.7cm}
      \input{schema/geocentriques_2d.tex}
      Latitude géocentrique: $\omega$ \\
      Angle entre le plan équatorial et la droite $(OM)$
    \end{column}
    \begin{column}{.55\linewidth}
      Sous l'hypothèse de Terre sphérique :
      $$\psi = \omega$$
      \psset{unit=.95cm}
      \vfill
      \centerline{\input{schema/geocentriques_3d.tex}}
    \end{column}
  \end{columns}
\end{frame}


\begin{frame}
  \frametitle{Coordonnées géographiques}
  \begin{columns}
    \begin{column}{.5\linewidth}
      \psset{unit=.8cm}
      \centerline{\input{schema/geographiques_2d.tex}}
    \end{column}
    \begin{column}{.5\linewidth}
      $\varphi$: Latitude géographique, angle entre le plan équatorial et le vecteur normal. \\
      $(\lambda, \varphi)$: Coordonnées géographiques, ce sont les coordonnées usuelles (GPS).
    \end{column}
  \end{columns}
\end{frame}


\begin{frame}
  \frametitle{Mesure de la latitude géographique}
  \begin{columns}
    \begin{column}{.5\linewidth}
      Mesure de l'angle entre la verticale et une étoile fixe.
    \end{column}
    \begin{column}{.5\linewidth}
      \psset{unit=.8cm}
      \centerline{\input{schema/geographiques_star.tex}}
    \end{column}
  \end{columns}
\end{frame}


\begin{frame}
  \frametitle{Comparaison des différentes latitudes}
  \begin{columns}
    \begin{column}{.4\linewidth}
      \psset{unit=.7cm}
      \centerline{\input{schema/comparatif_latitude.tex}}
    \end{column}
    \begin{column}{.6\linewidth}
      Sur l'ellipsoïde WGS84
      $$\left\{ \begin{array}{l}
          a=\SI{6378137.0}{\meter} \\
          b=\SI{6356752.314}{\meter}
        \end{array}\right.$$
      \footnotesize
      \begin{tabular}{rrr}
        \hline
        Latitude & Latitude & Latitude \\
        paramétrique & géocentrique & géographique \\
        $\psi$ & $\omega$ & $\varphi$ \\
        \hline
        \expandableinput include/table_comparatif_latitude.tex
        \hline
      \end{tabular}
    \end{column}
  \end{columns}
\end{frame}


\begin{frame}
  \frametitle{Modélisation de la surface terrestre: le géoïde}
  \vspace{-3pt}
  \begin{itemize}
  \item La terre n'est pas un solide homogène \\
    $\implies$ Présence d'anomalies du champ de pesanteur.
  \item Géoïde: surface équipotentielle du champ de pesanteur \\
    $\implies$ Représente l'horizontale.
  \item Par convention, le géoïde coïncide avec le niveau moyen des mers.
  \end{itemize}
  \begin{columns}
    \begin{column}{.45\linewidth}
      \psset{unit=.4cm}
      \centerline{\input{schema/geoide.tex}}
    \end{column}
    \begin{column}{.55\linewidth}
      \includegraphics[width=\linewidth]{images/geoids_sm.eps}
    \end{column}
  \end{columns}
\end{frame}


\begin{frame}
  \frametitle{Géoïde EGM96}
  \includegraphics[width=\linewidth]{images/egm96_mollweide.eps}
\end{frame}


\begin{frame}
  \frametitle{Différence entre ellipsoïdes}
  \vspace{-5pt}
  \begin{itemize}
  \item Paramètres de plusieurs ellipsoïdes.
    Un ellipsoïde est définit par $a$ et $b$ ou $a$ et $1/f$, en gris, valeur calculée.
    {\scriptsize
      \centerline{
        \begin{tabular}{lS[table-format=7.1]
                         S[table-format=7.3]
                         S[table-format=3.9]}
          \toprule
          Nom & {$a$ (\si{\meter})} & {$b$ (\si{\meter})} & {$1/f$} \\
          \midrule
          \expandableinput include/table_ellipsoid_defs.tex
          \bottomrule
        \end{tabular}
      }
    }
    \vfill
  \item Variation de lattitude selon l'ellipsoïde.
    Tous les ellipsoïdes sont pris avec le même centre. \\
    Une différence de \SI{1}{\arcminute} correspond \`a un écart de \SI{30}{\meter} au niveau de l'équateur.
    {\scriptsize
      \centerline{
        \begin{tabular}{lrrr}
          \toprule
          Nom & \multicolumn{3}{c}{Latitudes} \\
          \midrule
          \expandableinput include/table_ellipsoid_latitude.tex
          \bottomrule
        \end{tabular}
      }
    }
  \end{itemize}
\end{frame}


\subsection{Altitude}


% \begin{frame}
%   \frametitle{3\ieme dimension: Altitude}
%   \begin{itemize}
%   \item Verticale définie par la direction du vecteur pesanteur \\
%     $\implies$ Direction du fil \`a plomb.
%   \item
%   \end{itemize}
% \end{frame}


\begin{frame}
  \frametitle{Altitude orthométrique}
  \vspace{-5pt}
  Hauteur orthométrique: Longueur de la ligne de force du champ de pesanteur au dessus du géoïde.
  \psset{unit=.8cm}
  \centerline{\input{schema/altitude_defs.tex}}
\end{frame}
