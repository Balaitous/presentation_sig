DOC=sig
all: $(DOC).pdf

sig-notes.tex: sig.tex
	echo "\PassOptionsToClass{handout}{beamer}" > $@
	cat $< >> $@

IMGDIR=images

PNG = \
	images/QGis_Logo.png \
	images/arome_t2m_lbrt93.png \
	images/arome_t2m_wgs84.png \
	images/carte_babylonienne_schema.png \
	images/gdal_logo.png \
	images/google-am.png \
	images/google-am2.png \
	images/google-argentine.png \
	images/google-chili.png \
	images/google-maroc-france-label.png \
	images/google-maroc-france.png \
	images/google-maroc-label.png \
	images/google-maroc.png \
	images/map-distances-mercator-africa-russia.png \
	images/paris-geofabrik-2006.png \
	images/paris-geofabrik-2007.png \
	images/paris-geofabrik-2008.png \
	images/paris-geofabrik-2009.png \
	images/qgis_gui.png \
	images/sr_epsg_2154.png

JPG = \
	images/AMDAR_geoloc.jpg \
	images/atlas_catalan.jpg \
	images/atlas_ortelius.jpg \
	images/bulletin_meteo_2015_03_08.jpg \
	images/cadastre_orange.jpg \
	images/cadastre_orange_detail.jpg \
	images/cadastre_orange_lignes.jpg \
	images/cadastre_orange_plan_situation.jpg \
	images/cadastre_orange_salle.jpg \
	images/carte_mines.jpg \
	images/carte_babylonienne.jpg \
	images/carte_babylonienne_interpretation.jpg \
	images/carte_eratostene.jpg \
	images/carte_ptolemee.jpg \
	images/cassini_assemblage.jpg \
	images/cassini_toulouse.jpg \
	images/cassini_toulouse_detail.jpg \
	images/cassini_triangulation.jpg \
	images/cassini_triangulation_detail.jpg \
	images/dalle_ornee_saint_bellec.jpg \
	images/dalle_ornee_saint_bellec_description.jpg \
	images/dalle_ornee_saint_bellec_vallee.jpg \
	images/egm96_mollweide.jpg \
	images/etat_major_luchon.jpg \
	images/etat_major_toulouse.jpg \
	images/etat_major_toulouse_detail.jpg \
	images/etat_major_triangulation.jpg \
	images/france_1553_nova_totius.jpg \
	images/geoids_sm.jpg \
	images/google-earth_3d.jpg \
	images/google-earth_global.jpg \
	images/mercator-afrique.jpg \
	images/osm-foix.jpg \
	images/osm-foix_cycle.jpg \
	images/osm-foix_mapquest.jpg \
	images/osm-foix_wanderreitkarte.jpg \
	images/sr_logo.jpg \
	images/tissot_bonne.jpg \
	images/tissot_conique_equidistance.jpg \
	images/tissot_cylindrique_equidistance.jpg \
	images/tissot_eckert_IV.jpg \
	images/tissot_lonlat.jpg \
	images/tissot_mercator.jpg \
	images/tissot_mollweide.jpg \
	images/tissot_sinusoidale.jpg \
	images/tissot_stereographic_north.jpg \
	images/tissot_van_der_grinten.jpg \
	images/vaugondy_1750.jpg

GIF=

PDF = \
	images/CC-9zones.pdf \
	images/RR.pdf \
	images/T2m.pdf \
	images/aster.pdf \
	images/corine.pdf \
	images/densite1.pdf \
	images/densite2.pdf \
	images/grid_example.pdf \
	images/met10_201403121200.pdf \
	images/meteopole.pdf \
	images/rivers.pdf \
	images/scan1000.pdf \
	images/tempe_dist.pdf \
	images/tempe_krige.pdf \
	images/th_france.pdf \
	images/villes1.pdf \
	images/villes2.pdf

SVG = \
	images/Cc-by-nc-nd_icon.svg \
	images/Cc-by-nc-sa_icon.svg \
	images/coords_spheriques.svg

# Illustrations asy 2D
ASY_2D = \
	schema/projection_conique_2d.asy \
	schema/projection_conique2_2d.asy \
	schema/projection_stereographique_2d.asy \
	schema/projection_cylindrique_2d.asy \
	schema/projection_transverse_mercator_2d.asy \
	schema/utm.asy \
	schema/utm_france.asy

# Illustrations asy 3D
ASY_3D = \
	schema/projection_conique.asy \
	schema/projection_conique2.asy \
	schema/projection_stereographique.asy \
	schema/projection_cylindrique.asy \
	schema/projection_transverse_mercator.asy

# Illustrations asy
ASY = $(ASY_2D) $(ASY_3D)

# Liste des schéma pstricks
LATEX_PSTRICKS = \
	schema/altitude_defs.tex \
	schema/comparatif_latitude.tex \
	schema/gravity.tex \
	schema/geoide.tex \
	schema/geocentriques_2d.tex \
	schema/geocentriques_3d.tex \
	schema/geographiques_2d.tex \
	schema/geographiques_star.tex \
	schema/parametriques_2d.tex \
	schema/parametriques_3d.tex \
	schema/referentiel.tex

# Toutes les dépendances d'images en eps
LATEX_EPS = \
	$(PNG:.png=.eps) \
	$(JPG:.jpg=.eps) \
	$(GIF:.gif=.eps) \
	$(PDF:.pdf=.eps) \
	$(SVG:.svg=.eps) \
	$(ASY:.asy=.eps) \
	images/dalle_ornee_pailler_2016.eps

# Code latex inclu
LATEX_INCLUDE = \
	include/py-default-style.tex \
	include/epsg_2154.tex \
	include/osm-foix.tex \
	include/gdalinfo.tex \
	include/kriging.tex \
	include/ogrinfo.tex \
	include/r_plot.tex \
	include/r_sum.tex \
	include/synop_data.tex \
	include/synop_data_vrt.tex \
	include/synop_data_result.tex \
	include/synop_data_db.tex \
	include/table_comparatif_latitude.tex \
	include/table_ellipsoid_defs.tex \
	include/table_ellipsoid_latitude.tex
clean-latex-include:
	rm -f $(LATEX_INCLUDE)

# Parties de la présentation
LATEX_PARTS = intro.tex maps.tex coordinates.tex projections.tex symbologie.tex data.tex tools.tex

LATEXFLAGS = -shell-escape

# Liste de toutes les dépendences du document latex
LATEXDEPS = $(LATEX_EPS) $(LATEX_PSTRICKS) $(LATEX_INCLUDE) $(LATEX_PARTS)
echo_deps:
	echo $(LATEXDEPS)

CLEAN=aux dvi ps out toc log nav snm vrb
CLEANALL=pdf

# Environnement python
PY-VENV = py-venv
py-activate = py-venv/bin/activate
export PYTHON = env PYTHONPATH=$(abspath scripts) VIRTUAL_ENV=$(abspath $(PY-VENV)) \
	$(abspath $(PY-VENV))/bin/python3

$(py-activate): requirements.txt
	python3 -m venv $(PY-VENV)
	$(PYTHON) -m pip install -r $<
	ln -sf `python3 -c "import osgeo, os; print(os.path.dirname(osgeo.__file__))"` \
		`$(PYTHON) -c "import site; print(site.getsitepackages()[0])"`

# Desactive les regles implicites
.SUFFIXES:
# Ne supprime pas les images intermediaires
.PRECIOUS: %.eps %.ps %.png %.nav

# Téléchargeent des données

# trait de côte
data/ne_110m_coastline.zip:
	wget -O $@ https://naciscdn.org/naturalearth/110m/physical/$(notdir $@) \
	|| (rm -f $@ && false)
data/ne_110m_coastline.shp: data/ne_110m_coastline.zip
	cd data && unzip $(notdir $<)
	touch $@

# Conversion automatique d'images
%.eps: %.png
	convert $< -strip $@

%.eps: %.jpg
	convert $< -strip $@

%.eps: %.gif
	convert $< -strip $@

%.eps: %.pdf
	pdftops -eps $< $@

%.eps: %.svg
	inkscape --export-type=eps --export-filename=$@ $<

%.nav: %.tex $(LATEXDEPS)
	latex $(LATEXFLAGS) $<

%.dvi: %.nav $(LATEXDEPS)
	latex $(LATEXFLAGS) $*.tex

%.ps: %.dvi
	dvips -o $@ $<

%.pdf: %.ps
	ps2pdf -dALLOWPSTRANSPARENCY $<

# Génération des schémas Asymptote
PROJECTIONS_DEPS = schema/coastline.asy schema/earth.asy schema/projections.asy

ASY_CMD = env ASYMPTOTE_DIR=$(abspath schema) asy

schema/coastline.asy: data/coastline.shp scripts/shp2asy.py $(py-activate)
	$(PYTHON) scripts/shp2asy.py --path coastline $< > $@ || (rm -f $@ && false)

$(ASY_3D:.asy=.jpg): %.jpg: %.asy $(PROJECTIONS_DEPS)
	$(ASY_CMD) -render 8 -noprc -f jpg -o $@ $<

$(ASY_3D:.asy=.eps): %.eps: %.jpg
	convert $< -strip $@

$(ASY_2D:.asy=.eps): %.eps: %.asy $(PROJECTIONS_DEPS)
	$(ASY_CMD) -render 0 -f eps -o $@ $<

$(ASY_2D:.asy=.jpg): %.jpg: %.eps
	convert -density 300 -antialias $< $@

# Génaration des scripts
PYG_FLAGS = -f tex -O commandprefix=PYGdefault
PYGMENT = $(PYTHON) -m pygments

include/py-default-style.tex: $(py-activate)
	$(PYGMENT) -S default -f latex -O commandprefix=PYGdefault > $@ || (rm -rf $@ && false)

include/synop_data.tex: include/synop_data.txt $(py-activate)
	$(PYGMENT) $(PYG_FLAGS) -l text -o $@ $<

include/synop_data_db.tex: include/synop_data_db.txt $(py-activate)
	$(PYGMENT) $(PYG_FLAGS) -l text -o $@ $<

include/synop_data_vrt.tex: include/synop_data.vrt $(py-activate)
	$(PYGMENT) $(PYG_FLAGS) -l xml -o $@ $<

include/synop_data_result.tex: include/synop_data_result.txt $(py-activate)
	$(PYGMENT) $(PYG_FLAGS) -l text -o $@ $<

include/gdalinfo.tex: include/gdalinfo.bash $(py-activate)
	$(PYGMENT) $(PYG_FLAGS) -l text -o $@ $<

include/ogrinfo.tex: include/ogrinfo.bash $(py-activate)
	$(PYGMENT) $(PYG_FLAGS) -l text -o $@ $<

include/r_plot.tex: include/r_plot.console $(py-activate)
	$(PYGMENT) $(PYG_FLAGS) -l rout -o $@ $<

include/r_sum.tex: include/r_sum.R $(py-activate)
	$(PYGMENT) $(PYG_FLAGS) -l r -o $@ $<

include/kriging.tex: include/kriging.R $(py-activate)
	$(PYGMENT) $(PYG_FLAGS) -l r -O linenos=True -o $@ $<

include/osm-foix.tex: data/osm-foix.vrt $(py-activate)
	$(PYGMENT) $(PYG_FLAGS) -l xml -o $@ $<

include/epsg_2154.tex: include/epsg_2154.wkt scripts/pygment_wkt.py $(py-activate)
	$(PYTHON) scripts/pygment_wkt.py $< > $@

include/table_comparatif_latitude.tex: scripts/comparatif_latitude.py $(py-activate)
	$(PYTHON) scripts/comparatif_latitude.py > $@

include/table_ellipsoid_latitude.tex: \
		scripts/ellipsoid_latitudes.py scripts/commun_ellipsoid.py $(py-activate)
	$(PYTHON) scripts/ellipsoid_latitudes.py > $@

include/table_ellipsoid_defs.tex: \
		scripts/ellipsoid_defs.py scripts/commun_ellipsoid.py $(py-activate)
	$(PYTHON) scripts/ellipsoid_defs.py > $@

view: $(DOC).pdf
	okular $@

tar:
	rm -r $(DOC)
	mkdir $(DOC)
	mkdir $(DOC)/images
	cp Makefile hyperendnote.sty $(DOC).tex $(DOC)
	cp $(addprefix images/, $(PNG) $(JPG)) $(DOC)/images
	tar -czf $(DOC).tar.gz $(DOC)
	rm -r $(DOC)
