\documentclass[xcolor={dvipsnames,svgnames}]{beamer}

\usepackage[utf8]{inputenc}
\usepackage{etex}
\usepackage[francais]{babel}
\usepackage[super]{nth}
\usepackage[authoryear]{natbib}
\usepackage{pstricks}
\usepackage{pstricks-add}
\usepackage{pst-3dplot}
\usepackage{pst-eucl}
\usepackage{pst-poly}
\usepackage{pst-text}
\usepackage[cache=false]{minted} % Formatage des sorties pygmentize
\usepackage{multicol}
\usepackage{booktabs}
\usepackage{siunitx}
\usepackage{hyperref}
\usepackage[strict]{changepage}

%% Ajout des notes sur toutes les pages
% https://tex.stackexchange.com/questions/141038/beamer-section-page-and-show-notes-for-every-page
\makeatletter
\def\beamer@framenotesbegin{% at beginning of slide
  \gdef\beamer@noteitems{}%
  \gdef\beamer@notes{{}}% used to be totally empty.
}
\makeatother

\setbeamertemplate{note page}{\insertnote}

%% Options pour la version annotée
\mode<handout>{%
  \usepackage{pgfpages}
  \pgfpagesuselayout{4 on 1}[a4paper,landscape,border shrink=5mm]
  \setbeameroption{show notes}
}

% Style pygmentize
\input{include/py-default-style.tex}

%% Configuration pour siunitx
% Locale
\sisetup{locale = FR}
% Ajout des noueds aéronautique
\DeclareSIUnit\knot{kt}
% Formatage des angles
\ExplSyntaxOn
\cs_new:Npn \__angle_print:nnn #1#2#3 {
  \IfNoValueF{#1}
    {\SI{#1}{\degree}}
  \IfNoValueF{#2}
    {\SI[minimum-integer-digits=2]{#2}{\arcminute}}
  \IfNoValueF{#3}
    {\SI[minimum-integer-digits=2]{#3}{\arcsecond}}
}
\NewDocumentCommand \azimuth { > { \SplitArgument { 2 } { ; } } m } {
  \__angle_print:nnn #1
}
\ExplSyntaxOff

\usetheme{Warsaw}

\title[Introduction SIG]{Introduction aux Syst\`emes d'information g\'eographique}
\author{Alain Delplanque (\scriptsize\url{alain.delplanque@meteo.fr})}
\institute{M\'et\'eo-France}
\date{Mars 2024}


\setcounter{tocdepth}{2}

\usecolortheme{crane}

% Les images de base
\RequirePackage{pgf}
\pgfdeclareimage[width=\paperwidth]{mf-cover}{images/theme/diapo_type_mf_cover}
\pgfdeclareimage[width=\paperwidth]{mf-all}{images/theme/diapo_type_mf}
\pgfdeclareimage[width=\paperwidth]{mf-notitle}{images/theme/diapo_type_mf_notitle}
\pgfdeclareimage[width=0.17034\paperwidth]{mf-mini}{images/theme/diapo_type_mf_notitle_nofoot}
\pgfdeclareimage[width=1.00000\paperwidth]{mf-haut}{images/theme/element_type_mf_haut1_00}
\pgfdeclareimage[width=0.43747\paperwidth]{mf-bas}{images/theme/element_type_mf_bas0_433}
%% \pgfdeclareimage[width=0.04\paperwidth]{cc-by-nc-nd}{images/Cc-by-nc-nd_icon.eps}

% Blocs arrondis
\setbeamertemplate{blocks}[rounded][shadow=true]
\setbeamercovered{transparent}

% Couleurs
\definecolor{beamer@blendedblue}{rgb}{0.000,0.439,0.753}
\definecolor{mfgrey}{rgb}{0.250,0.250,0.250}
\setbeamercolor{frametitle}{fg=beamer@blendedblue}
\setbeamercolor{alerted text}{fg=orange}
\setbeamercolor{block title}{fg=mfgrey,bg=beamer@blendedblue}
\setbeamercolor{block title}{use=structure,fg=white,bg=structure.fg!80!black}
\setbeamercolor{block title alerted}{use=alerted text,fg=white,bg=alerted text.fg!80!black}
\setbeamercolor{block title example}{use=example text,fg=white,bg=example text.fg!80!black}
\setbeamercolor{whitetext}{fg=white,bg=beamer@blendedblue!80!black}
% Couleurs page de titre
\setbeamercolor{title}{fg=beamer@blendedblue}
\setbeamercolor{subtitle}{fg=white!70!black}
\setbeamercolor{author}{fg=white!70!black,bg=white}
\setbeamercolor{institute}{fg=white!70!black}
\setbeamercolor{date}{fg=white!70!black}

% Polices d'ecriture
\setbeamerfont{section in head/foot}{size=\tiny,series=\normalfont}
\setbeamerfont{frametitle}{size=\large}
\setbeamerfont{subsection in toc}{size=\small}
\setbeamerfont{subsubsection in toc}{size=\small}
% Polices d'ecriture page de titre
\setbeamerfont{title}{size=\large,parent=structure}
\setbeamerfont{subtitle}{size=\normalsize,parent=title}
\setbeamerfont{author}{size=\footnotesize}
\setbeamerfont{institute}{size=\tiny}
\setbeamerfont{date}{size=\footnotesize}

% Virer la barre de navigation
\setbeamertemplate{navigation symbols}{}

% Fond d'ecrean standard
\usebackgroundtemplate{\pgfuseimage{mf-all}}

% ------------------------------------------------------------------------
% Affichage du titre
\setbeamertemplate{frametitle}{%
  \vskip-0.075cm
  \hbox{\hskip1cm\begin{beamercolorbox}[leftskip=2.5cm,sep=0em,wd=.8\paperwidth,left,rounded=true]{frametitle}
    \hskip.5cm\insertframetitle
  \end{beamercolorbox}}
  \vskip0.3cm
}

% ------------------------------------------------------------------------
% En tete (plan)
\setbeamertemplate{headline}{%
  \pgfuseimage{mf-haut}
  \vskip-1.40cm
  \linethickness{0pt}
  \framelatex{
    \begin{beamercolorbox}[wd=\paperwidth,ht=0.6cm]{Title bar}
      \hskip2.22cm\insertsectionnavigationhorizontal{0pt}{}{}%
  \end{beamercolorbox}}
}

% ------------------------------------------------------------------------
% Pied de page
\setbeamertemplate{footline}{%
  \flushright\pgfuseimage{mf-bas}
  \vskip-0.5cm
  \linethickness{0pt}
  \framelatex{
    \begin{beamercolorbox}[leftskip=.3cm,wd=.42\paperwidth,ht=0.5cm,sep=0.1cm]{Location bar}
      %% \pgfuseimage{cc-by-nc-nd}\hspace{5pt}
      \insertshorttitle
      \hfill
      \insertframenumber/\inserttotalframenumber
      \vskip0.1cm
    \end{beamercolorbox}
  }
}

% Page de titre
\setbeamertemplate{title page}{%
  \begin{center}
    \begin{beamercolorbox}[center,shadow=true,rounded=true,sep=4pt]{title}
      \usebeamerfont{title}\inserttitle\par%
      \ifx\insertsubtitle\@empty%
      \else%
      \vskip0.25em%
             {\usebeamerfont{subtitle}\usebeamercolor[fg]{subtitle}\insertsubtitle\par}%
             \fi%
    \end{beamercolorbox}%
    \vskip0.9em\par
    \psframebox*[fillcolor=white,opacity=.5]{\usebeamercolor{author}\usebeamerfont{author}\insertauthor} \\
    \vskip0.9em\par
 	\psframebox*[fillcolor=white,opacity=.5]{\usebeamercolor{institute}\usebeamerfont{institute}\insertinstitute} \\
    \vskip0.9em\par
 	\psframebox*[fillcolor=white,opacity=.5]{\usebeamercolor{date}\usebeamerfont{date}\insertdate} \\
  \end{center}
}

% Insertion du sommaire après chaque section
\AtBeginSection[]{%
  \begin{frame}{Table of content}
    \tableofcontents[currentsection, hideothersubsections]
  \end{frame}
}

% Réglage des listes et enumerations
\setbeamertemplate{sections/subsections in toc}[square]
\setbeamertemplate{items}[default]

% configuration pstricks
\psset{arrowsize=6pt}
\psset{unit=1cm}
\SpecialCoor


% Configuration hyperref
\hypersetup{colorlinks=true,allcolors=,urlcolor={RoyalBlue}}

%% Problème d'import des tables
%% cf: https://tex.stackexchange.com/questions/144625/misplaced-noalign-error-in-table-but-only-when-using-include
\makeatletter\let\expandableinput\@@input\makeatother

\begin{document}

\setbeamertemplate{background canvas}{%
  \includegraphics[width=\paperwidth,height=\paperheight]{images/theme/fond_fin.eps}}
\maketitle
\setbeamertemplate{background canvas}{}

% -----Licence et crédit-----
%% \begin{frame}
%%   \vspace{50pt}
%%   \hfill
%%   \parbox{.8\linewidth}{
%%     Cette pr\'esentation est plac\'ee sous licence
%%     \href{
%%       https://creativecommons.org/licenses/by-nc-nd/3.0/fr/}{
%%       \includegraphics[width=1.25cm]{images/Cc-by-nc-nd_icon.eps}}. \\
%%     Les logos M\'et\'eo-France, les illustrations de premi\`ere et derni\`ere page,
%%     les illustrations provenant de parties tierces
%%     sous soumis \`a leurs droit d'auteur respectifs
%%     (\href{https://gitlab.com/Balaitous/presentation_sig/blob/master/LICENCE.md}{%
%%       Cr\'edit des illustrations}). \\
%%     Les autres composants de cette pr\'esentation sont individuellement plac\'es sous licence
%%     \href{
%%       https://creativecommons.org/licenses/by-nc-sa/3.0/fr/}{
%%       \includegraphics[width=1.25cm]{images/Cc-by-nc-sa_icon.eps}}.
%%   }
%%   \parbox{.05\linewidth}{}
%%   \vspace{30pt} \\
%%   Pr\'esentation r\'ealis\'ee \`a l'aide de \LaTeX\ et beamer. \\
%%   \url{https://gitlab.com/Balaitous/presentation_sig/tree/master}
%% \end{frame}
% ---------------------------


% --------- Sommaire ---------
\begin{frame}{Table of content}
  \tableofcontents
\end{frame}
% ----------------------------

\def\siecle#1{\textsc{\romannumeral #1}\textsuperscript{e}~si\`ecle}

\input{intro.tex}
\input{maps.tex}
\input{coordinates.tex}
\input{projections.tex}
\input{symbologie.tex}
\input{data.tex}
\input{tools.tex}

\setbeamertemplate{background canvas}{%
  \includegraphics[width=\paperwidth,height=\paperheight]{images/theme/fond_fin.eps}}
\begin{frame}
  \null\vfill
  \hfill \LARGE \white Fin \hspace{2cm}\null
  \vfill\null
\end{frame}
\setbeamertemplate{background canvas}{}

\end{document}
